<?php



 
class pdo_connect {
 
	private static $_instance;
 
	public static function pdo_connection() {
    
        $HOST='127.0.0.1';
	
		$PARAM_nom_bd='theurbexbook'; // le nom de votre base de données
		$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
		$PARAM_mot_passe=''; // mot de passe de l'utilisateur pour se connecter
		self::$_instance =  new PDO("mysql:host=$HOST;dbname=$PARAM_nom_bd", $PARAM_utilisateur, $PARAM_mot_passe, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)); // connexion à la BDD

       return self::$_instance;
    }
    


}



class bdd extends pdo_connect{

	private $_bdd;

	function __construct(){
		$this->_bdd = pdo_connect::pdo_connection();
	}

	

	function find($table, $value="*", $cond="", $join="",$group=""){
		if ($value == "") {
			
			$value ="*";
		}

		$select="";
		$from="";
		$where="";
		$groupby="";


		/* ----- SELECT ----- */
		;

		if (is_array($value)){ 
		
			foreach ($value as $toAdd){ $select .= ", ".$toAdd; }
			$select=substr($select, 1);
		}

		else{  $select .= $value; }
		
		$select = "SELECT ".$select;
	
		/* ----- FROM ----- */
		
		if (is_array($table)){
			foreach ($table as $toAdd){ $from .= ",".$toAdd; }
			$from= substr($from, 1);
		}

		else{ $from .= $table; }
		$from = " FROM ".$from;
		



		/* ----- WHERE ----- */
		if ($cond != "" || $join != ""){
			$where=" WHERE ";
			if ($cond != "" ){
				if (is_array($cond)){
						foreach ($value as $toAdd){ $where .= $toAdd." AND "; }
						
				}
				else $where .= $cond." AND ";

			}

			if ($join != "" ){
				foreach ($join as $key=> $value) {
					$where.= "id_".$value."_pk = id_".$value."_fk AND ";
				}
				
			}
		$where= substr($where, 0, -4);
	
		}

		/* ----- GROUP BY ----- */

		if ($group != ""){
		
			$groupby="GROUP BY (".$group.")";
		}







		$sql = $select . $from . $where . $groupby;
		
		$query = $this->_bdd->prepare($sql);
        	
       	$query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
	}


	function del($table,$cond=array()){

		$from="";
		$where="";
		$sql="";

		if (is_array($table)){
			foreach ($table as $toDel){ $from .= ",".$toDel; }
			$from= substr($from, 1);
		}

		else{ $from .= $table; }
		$from = " FROM ".$from;



		foreach ($cond as $key => $value){ $where .= $key." = ". $value. " AND "; }
			$where= substr($where ,0, -4);
		

		

		




		$sql="DELETE ". $from . " WHERE " . $where;
		$query = $this->_bdd->prepare($sql);
        	
       	$query->execute();
		return TRUE;
	}

	function add($table,$val=array()){
		$values="";
		$col="";
		$sql="";

		foreach ($val as $key => $value) {
			$values .= "'$value', ";
			$col .="$key, ";
		}
		$values = substr($values, 0, -2);
		$col = substr($col, 0, -2);
		$sql= "INSERT INTO $table ($col) VALUES ($values)";
		//echo $sql;
		$query = $this->_bdd->prepare($sql);
        	
       	$query->execute();


		return $this->_bdd->lastInsertId(); 

	}


}