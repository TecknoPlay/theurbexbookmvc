<?php

/* ------------  ROUTEUR DU MVC ---------------------------- */

define('WEBROOT', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
require_once (WEBROOT.'controleur/bootstrap.php');
require_once (WEBROOT.'model/bdd.php');



$url_split = explode('/', $_GET['p']);
if (!isset($url_split[0])) $url_split[0]="";
if (!isset($url_split[1])) $url_split[1]="";
if (!isset($url_split[2])) $url_split[2]="";
if (!isset($url_split[3])) $url_split[3]="";

$controleur=$url_split[0];
$method=$url_split[1];
$param=$url_split[2];
$parm2=$url_split[3];

if ($controleur !='' || $method!='' ){


	if (file_exists(ROOT.'controleur/'.$controleur.".php") == TRUE){

		require (WEBROOT.'controleur/'.$controleur.".php");
		if (method_exists($controleur, $method)){

			$page = new $controleur();
			$page->$method($param, $parm2);
		}

		else{
			
			require (WEBROOT.'controleur/page.php');
			$page = new page();
			$page->error('1045');
		}
	}

	else{
		
		require (WEBROOT.'controleur/page.php');
		$page = new page();
		$page->error('1040');
		}
	}
else{
	require (WEBROOT.'controleur/page.php');
	$page = new page();
	$page->login();
}




?>