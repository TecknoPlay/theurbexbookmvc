Travail de PHP/SGBD de Jean Gervasi

Organisation du code: schema MVC

	Model:Tout le code qui a un rapport avec PDO et le stockage d'info
	View: Tout les documents HTML/CSS/JS. Les seuls code PHP sont des foreach des tableaux envoy� par le controleur
	Controleur: Les php, le controleur fait la liaison entre les vue et le model. Il envoie ses resultats a la vue grace a un tableau


INDEX.PHP
Fichier de routage, fait appel au bon controleur en fonction de l'url
127.0.0.1/afficher/album/Val Benoit/75
  -----    ------   ---   --------  ---
    |         |      |       |       |
url du site   |      |       |       |
          nom du controleur  |       |
                     |       |       |
                   nom de la methode |
                             |       |
                       parametre 1   |
                                parametre 2 

METHODE find($table, $value, $cond, $join, $group)
	- $table : nom de(s) table(s), si plusieurs -> passage en array()
	- $value : champs a afficher, de base *, si plusieurs -> passage en array()
	- $cond  : condition supplementaire, si plusieurs -> passage en array()
	- $join  : si plusieurs table, les cl�s a joindre, passage obblig� en array()
	- $group : grouper les resultat, passage d'une seul info en str