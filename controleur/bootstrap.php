<?php

class Controller{
	var $vars =array();
	var $isLog=1;

	function __construct(){
		if ($_SERVER['PHP_SELF'] != '/pages/add_user.php' && $_SERVER['PHP_SELF'] != 'add_user.php'  ){
			
			if (isset ($_COOKIE['user']) && isset ($_COOKIE['auth']) && isset ($_COOKIE['photographe'])){
				$token=sha1(md5(md5($_COOKIE['user'])+md5($_COOKIE['auth']) + md5($_COOKIE['photographe']))+8+md5("theurbexbook"));
				
				$this-> loadModel('bdd');
				$userInfo=$this->bdd->find('user','token',"id_user_pk = '".$_COOKIE['user']."'");
				
		
				
				if ($userInfo[0]->token != $token){
					
					setcookie('user', NULL, -1,"/");
					setcookie('auth', NULL, -1,"/");
					setcookie('photographe', NULL, -1,"/");
					@session_destroy();
					$this->isLog = 0;
					
				}

			}
			else{
				setcookie('user', NULL, -1,"/");
				setcookie('auth', NULL, -1,"/");
				setcookie('photographe', NULL, -1,"/");
				@session_destroy();
				$this->isLog = 0;
				
			}
		}
		echo $this->isLog;
	}

	function set($d){

		
		if ($this->isLog ==0) unset($d['edit_menu']);
		$this->vars =  $d;
	}

	function render($filename,$layout="default"){
		extract($this->vars);
		define ('PATH', "/view/layout/$layout/");
		include(ROOT.'view/layout/'.$layout.'/top_menu.php');
		if ($this->isLog == 1) @include(ROOT.'view/layout/'.$layout.'/left_menu.php');
		if ($this->isLog == 0) @include(ROOT.'view/layout/'.$layout.'/left_menu_unLog.php');
		@include(ROOT.'view/layout/'.$layout.'/menu_right.php');
		@include(ROOT.'view/layout/'.$layout.'/menu_bottom.php');

		require(ROOT.'view/layout/'.$layout.'/layout.php');

		require(ROOT.'view/'.$filename.'.php');

		include(ROOT.'view/layout/'.$layout.'/footer.php');
		
		
		
		
	}



	function loadModel ($name){
		require_once (ROOT.'model/'.$name.'.php');
		$this -> $name = new $name();
	}

	function isLogged(){
		if ($this->isLog == 1) return TRUE ;
		else return die(header ('Location: ../../page/login/'));
	} 
}