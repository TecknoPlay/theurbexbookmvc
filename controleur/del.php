<?php 

class del extends Controller{



	public function spot($spotName=""){

		$this-> isLogged();
		if ($spotName==""){
			$page_info=array (

				"title"=>"Supprimer un spot",
				"subtitle"=>"Selectionner le spot a supprimer",
				"desc"=>"Selectionner le spot a supprimer",
				"return"=>""

				);
			$edit_menu=array (
			"/add/spot/"=>"Ajouter un spot",
			"/edit/spot/"=>"Editer un spot",
			"/lister/spot/"=>"Retour a la liste"
			);
			$page_cat=array ();



			$this-> loadModel('bdd');
			
			$listSpot=$this->bdd->find(array('spot','type_spot'),'',"",array('type_spot'));

			$this->set(array(
						"spot_info" => $listSpot,
						"page_cat" => $page_cat,
						"edit_menu" => $edit_menu,
						"page_info" => $page_info
						));
			$this->render('del_spot');
		}

		if ($spotName!=""){
			$page_info=array (

				"title"=>"Supprimer un spot",
				"subtitle"=>"Selectionner le spot a supprimer",
				"desc"=>"Selectionner le spot a supprimer",
				"return"=>""

				);
			$page_cat=array ();



			$this-> loadModel('bdd');
			
			$delSpot=$this->bdd->del('spot',array("nom_spot"=>"'$spotName'"));
			$listSpot=$this->bdd->find(array('spot','type_spot'),'',"",array('type_spot'));

			$alert=array();
			if ($listSpot == TRUE)  $alert[0]="Le spot a bien été supprimé";

			$this->set(array(
						"spot_info" => $listSpot,
						"page_cat" => $page_cat,
						"alert" => $alert,
						"page_info" => $page_info
						));
			$this->render('del_spot');

		}

	}

	public function album($spotName, $albumId=""){
		$this-> isLogged();
		if ($albumId == "" ){

			$nomSpot=str_replace('_', ' ', $spotName );

	
			$page_info=array (
				"title"=>"$nomSpot",
				"subtitle"=>"Album de $nomSpot",
				"desc"=>"description d ela page",
				"return"=>"afficher/spot/$spotName"

				);
			$page_cat=array (
				"/lister/album/$spotName"=>"album",
				""=>"photo"

				);
			$edit_menu=array (
				"/add/album/$spotName/"=>"Ajouter un album",
				"/del/album/$spotName/"=>"Supprimer un album"
				);

			$this-> loadModel('bdd');
			$listAlbum=$this->bdd->find(array('photographe','album','spot','photos'),array('nom_photographe', 'prenom_photographe', 'url_mini', 'count(url_maxi) AS compteur','id_album_pk','nom_spot'),"nom_spot = '$spotName'",array('photographe', 'spot','album' ),'id_album_pk');

			$this->set(array(
						"album_info" => $listAlbum,
						"page_cat" => $page_cat,
						"page_info" => $page_info,
						"edit_menu" =>$edit_menu
						));
			$this->render('del_album');
		}
		if ($albumId != "" ){
			$page_info=array (

				"title"=>"Supprimer un Album",
				"subtitle"=>"Selectionner le spot a supprimer",
				"desc"=>"Selectionner le spot a supprimer",
				"return"=>""

				);
			$page_cat=array ();

			$edit_menu=array (
				"/add/album/$spotName/"=>"Ajouter un album",
				"/lister/album/$spotName/"=>"Retour a la liste"
				);

			$this-> loadModel('bdd');
			
			$delAlbum=$this->bdd->del('album',array("id_album_pk"=>"'$albumId'"));
			$listAlbum=$this->bdd->find(array('photographe','album','spot','photos'),array('nom_photographe', 'prenom_photographe', 'url_mini', 'count(url_maxi) AS compteur','id_album_pk','nom_spot'),"nom_spot = '$spotName'",array('photographe', 'spot','album' ),'id_album_pk');

			$alert=array();
			if ($listAlbum != "")  $alert[0]="L'album' a bien été supprimé";

			$this->set(array(
						"album_info" => $listAlbum,
						"page_cat" => $page_cat,
						"page_info" => $page_info,
						"edit_menu" =>$edit_menu
						));
			$this->render('del_album');
		}



	}


}

?>

