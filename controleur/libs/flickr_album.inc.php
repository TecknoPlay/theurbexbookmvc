<?php
function flickrAlbum($id, $user_id){


	$file= str_getcsv(file_get_contents("https://api.flickr.com/services/feeds/photoset.gne?set=".$id."&nsid=".$user_id."&lang=en-us&format=csv"));

	foreach ($file as $key => $value) {

		if ($key % 6 == 0){

			preg_match_all('#\bhttps?://farm[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $value, $match);

			if (isset($match[0][0])){
				$mini = $match[0][0];
				$maxi = str_replace("_m.jpg","_b.jpg",$mini); 
			


				$list_photo[$key]['mini'] = $mini;
				$list_photo[$key]['maxi'] = $maxi;
			}
		}

	}

	return $list_photo;


	
}


?>
