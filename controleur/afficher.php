<?php



class afficher extends Controller{
	function spot($spotName){

		$nomSpot=str_replace('_', ' ', $spotName );

		$page_info=array (
			"title"=>"$nomSpot",
			"subtitle"=>"titre de la page",
			"desc"=>"description d ela page"
			

			);
		$page_cat=array (
			"/lister/album/$spotName"=>"album",
			""=>"photo"
		

			);
		$edit_menu=array (
			"/edit/spot/$spotName"=>"Editer le spot",
			"/del/spot/$spotName"=>"Supprimer le spot"


			);

		$this-> loadModel('bdd');


		$infosSpot=$this->bdd->find(array( 
			'spot', 
			'type_spot',
			'etat_spot' )
		,array(
			'id_spot_pk',
			'nom_spot',
			'type',
			'etat_spot',
			'image_spot',
			'urbex_date_spot',
			'date_spot'),
		'nom_spot="'.$spotName.'"',
		array(
			'type_spot',
			'etat_spot'
			)



			);

		

		$this->set(array(
					"spot_info" => $infosSpot,
					"page_cat" => $page_cat,
					"page_info" => $page_info,
					"edit_menu" => $edit_menu
					));
		$this->render('spot');
	}

	function album($spotName,$id_album){
		$nomSpot=str_replace('_', ' ', $spotName );

		$page_info=array (
			"title"=>"$nomSpot",
			"subtitle"=>"Photo de : $nomSpot ",
			"desc"=>"description d ela page",
			"return"=>"afficher/spot/$spotName"

			);

		$page_cat=array (
			"/lister/album/$spotName"=>"album",
			""=>"photo"

		

			);

		$this-> loadModel('bdd');

		$photos=$this->bdd->find('photos','',"id_album_fk ='$id_album'");
		$this->set(array(
					"photos" => $photos,
					"page_cat" => $page_cat,
					"page_info" => $page_info
					));
		$this->render('album');

	}

}

?>