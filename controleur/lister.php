<?php

class lister extends Controller{

	public function spot($categorie=""){

		$page_info=array (

			"title"=>"titre de la page",
			"subtitle"=>"titre de la page",
			"desc"=>"description d ela page",
			"return"=>"lister/spot"

			);
		$page_cat=array (
			"industriel"=>"indus",
			"hopital"=>"hospi",
			"ecole"=>"ecole"

			);
		$edit_menu=array (
			"/add/spot/"=>"Ajouter un spot",
			"/edit/spot/"=>"Editer un spot",
			"/del/spot/"=>"Supprimer ue spot"
			);
		$this-> loadModel('bdd');
		if ($categorie=="")$listSpot=$this->bdd->find(array('spot','type_spot'),'',"",array('type_spot'));
		else $listSpot=$this->bdd->find(array('spot','type_spot'),'',"type='$categorie'",array('type_spot'));

		$this->set(array(
					"spot_info" => $listSpot,
					"page_cat" => $page_cat,
					"edit_menu" => $edit_menu,
					"page_info" => $page_info
					));
		$this->render('list_spot');

		

	}

	public function photographe(){
		$page_info=array (
			"title"=>"titre de la page",
			"subtitle"=>"titre de la page",
			"desc"=>"description d ela page"

			);
		$page_cat=array ();
		$this-> loadModel('bdd');
		$listSpot=$this->bdd->find('photographe','','');

		$this->set(array(
					"photographe_info" => $listSpot,
					"page_cat" => $page_cat,
					"page_info" => $page_info
					));
		$this->render('list_photographe');

		

	}
	public function album($spotName){
		$nomSpot=str_replace('_', ' ', $spotName );

	
		$page_info=array (
			"title"=>"$nomSpot",
			"subtitle"=>"Album de $nomSpot",
			"desc"=>"description d ela page",
			"return"=>"afficher/spot/$spotName"

			);
		$page_cat=array (
			"/lister/album/$spotName"=>"album",
			""=>"photo"

			);
		$edit_menu=array (
			"/add/album/$spotName/"=>"Ajouter un album",
			"/del/album/$spotName/"=>"Supprimer un album"
			);

		$this-> loadModel('bdd');
		$listAlbum=$this->bdd->find(array('photographe','album','spot','photos'),array('nom_photographe', 'prenom_photographe', 'url_mini', 'count(url_maxi) AS compteur','id_album_pk','nom_spot'),"nom_spot = '$spotName'",array('photographe', 'spot','album' ),'id_album_pk');

		$this->set(array(
					"album_info" => $listAlbum,
					"page_cat" => $page_cat,
					"page_info" => $page_info,
					"edit_menu" =>$edit_menu
					));
		$this->render('list_album');

		

	}



}

?>


	
