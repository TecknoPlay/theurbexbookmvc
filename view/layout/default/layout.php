<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" async type="text/css" href="<?php echo PATH; ?>css/style.css" > 
    <script type="text/javascript" src="<?php echo PATH; ?>js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo PATH; ?>js/jquery.form.min.js"></script>
        <link rel="stylesheet" async type="text/css" href="<?php echo PATH; ?>css/justifiedGallery.css" > 
    <link rel="stylesheet" async type="text/css" href="<?php echo PATH; ?>css/colorbox.css" > 

    <script type="text/javascript" src="<?php echo PATH; ?>js/justifiedGallery.js"></script>
    <script type="text/javascript" src="<?php echo PATH; ?>js/jquery.colorbox-min.js"></script>
    
    <script>

    
        $(document).ready(function () {
            $(".mygallery").each(function (i, el) {
                $(el).justifiedGallery({
                    rowHeight: 200,
                    rel: 'gal' + i
                }).on('jg.complete', function () {
                    $(this).find('a').colorbox({
                        maxWidth : '80%',
                        maxHeight : '80%',
                        opacity : 0.8,
                        transition : 'elastic',
                        current : ''
                    });
                });
            });
        });
    

    </script>

    <title><?php echo $page_info['title']; ?></title>

	<script>
		$(function() {

			var flag=0; 
			$( ".button_open_swipe_menu" ).click(function() {
				if (flag == 0) {
				  $( ".admin" ).animate({
					opacity: 1,
					 left: "0" 	
				}, 500, function() {
					flag=1;
 
				});
				}
				if (flag == 1) {
				  $( ".admin" ).animate({
					opacity: 1,
					 left: "-220" 	
				}, 500, function() {
					flag=0;
 
				});
				}
				
				
			});
		});
	
	</script>

    <script>
    
        var current_url = window.location.href;

        var link = $('.main-nav a[href='+current_url +']');
        link.css('background-image','images/activebg.png');

        alert (link);
       

    </script>

</head>

<body>
     
    <div class="site">
        <?php echo $top_menu; ?>
        <?php echo $left_menu; ?>
        <?php if (isset ($edit_menu)){ 
            echo "
                <h1>
                    <img width=\"30\" height=\"30\" alt=\"logo gestion\"  src=\"".PATH."/img/design/profile.png\">
                    Administation
                </h1>
                <ul>";
                foreach ($edit_menu as $key => $value) {
                   echo "<li><a href='$key'>$value</a></li>";
                }

            
                    
               echo" </ul>";



            } ?>
         
        <?php if (isset ($admin)){
                echo '
        <h1>
            <img width="30" height="30" alt="logo gestion"  src="<?php echo PATH; ?>/img/design/profile.png">
             

            
            Administation
        </h1>
        <ul>
            
             {block name=administration}<li>Aucune action 
             sur cette page</li>{/block}
           
        </ul>
        }';} ?>
        </div>
        <div class="content">
            <div class="barre_sous_sous_menu">
                
                    <div class="iconsoussousmenu" id="soussousselect">
                        <a href="<?php if (isset($page_info['return'])) echo '/'.$page_info['return'].'/'; ?>"><img width="23" alt= "home" height="23" src="<?php echo PATH; ?>img/design/home.png"></a>
                    </div>

                    
                
            
                <div class="titresoussousmenu">
                    <?php echo $page_info['title']; ?>
                </div>

                <?php foreach ($page_cat as $key => $value) {
                        echo'<div class="iconsoussousmenu">
                            <a class="toAnalyse" href="'.$key.'"><img width="23" alt= "'.$value.'" height="23" src="'. PATH .'img/design/'.$value.'.png"></a>
                        </div>';
                    }
                ?>

            </div>
        
            <div class="descsoussousmenu">
                 <?php echo $page_info['subtitle']; ?>
            </div>
            

